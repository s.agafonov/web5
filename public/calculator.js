function calculation() {
    var price=document.getElementById("price").value;
    var amount=document.getElementById("amount").value;
    var result;
    if(price=="") {alert("Вы не указали цену");}
    else if(amount=="") {alert("Вы не указали количество");}
    else {
        result=price*amount;
        document.getElementById("result").innerHTML="Стоимость заказа: "+result+" руб.";
    }
}

window.addEventListener('DOMContentLoaded', function (event) {
    console.log("DOM fully loaded and parsed");
    var button=document.getElementById("my-button");
    button.addEventListener("click", calculation);
  });
